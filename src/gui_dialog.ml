open Client_state
open Gui_common
open Utils

let join_game () =
    let dialog = GWindow.dialog ~title:("Rejoindre une partie")
        ~modal:true (* freeze the rest of the program *)
        ~allow_grow:true
        ~allow_shrink:false
        ~height:250
        () in
    let scroll = GBin.scrolled_window
        ~hpolicy:`NEVER ~vpolicy:`AUTOMATIC
        ~packing:(dialog#vbox#pack ~expand:true) () in
    let vbox = GPack.vbox ~packing:scroll#add_with_viewport () in
    let frame = GBin.frame ~label:("Serveur")
        ~border_width:3
        ~packing:vbox#pack () in
    let hbox = GPack.hbox ~spacing:7
        ~border_width:6
        ~packing:frame#add () in

    ignore (GMisc.label ~text:"Nom/Adresse"
              ~packing:(hbox#pack ~expand:false)());
    let entry_addr = GEdit.entry ~text:!addr
        ~max_length:80
        ~has_frame:true
        ~width:150
        ~packing:hbox#add () in
    ignore (GMisc.label
              ~text:"Port"
              ~packing:(hbox#pack ~expand:false) ());
    let entry_port = GEdit.entry ~text:(string_of_int !port)
        ~max_length:5
        ~width:50
        ~has_frame:true
        ~packing:(hbox#pack ~expand:false) () in

    let icon = GMisc.image ~icon_size:`MENU ~stock:`REFRESH () in
    let refresh = GButton.button~packing:(hbox#pack ~expand:false) () in
    refresh#set_image icon#coerce;

    let frame = GBin.frame ~label:("Parties")
        ~border_width:3
        ~packing:vbox#pack () in

    let cols = new GTree.column_list in
    let col_gamename = cols#add Gobject.Data.string
    and col_mapname = cols#add Gobject.Data.string
    and col_host = cols#add Gobject.Data.string
    and col_players = cols#add Gobject.Data.string
    and col_pwd = cols#add Gobject.Data.string
    in
    let model = GTree.list_store cols in
    let view = GTree.view ~model ~packing:frame#add () in
    let columns = [(col_gamename, "Nom");(col_mapname, "Carte");
                   (col_host, "Hôte");(col_players, "Joueurs");
                   (col_pwd, "Mot de passe")] in
    List.iter (fun (col, title) ->
        let renderer = GTree.cell_renderer_text [] in
        let vc = GTree.view_column
                   ~title ~renderer:(renderer, ["text", col])() in
        vc#set_clickable false;
        vc#set_resizable true;
        ignore (view#append_column vc)
      ) columns;

    let hhbox = GPack.hbox ~spacing:5 ~packing:dialog#action_area#add () in
    ignore (GMisc.label ~text:"Connexion :"
                        ~packing:(hhbox#pack ~expand:false)());

    let connexion_status =
      GMisc.image ~icon_size:`MENU ~stock:`NO
                        ~packing:(hhbox#pack ~expand:false)() in

    ignore (refresh#connect#clicked
              ~callback:(fun () ->
                connexion_status#set_stock `NO;
                let new_addr = entry_addr#text in
                let new_port = try int_of_string (entry_port#text)
                               with _ -> !port in
                if new_addr <> !addr || new_port <> !port
                then begin
                    addr := new_addr;
                    port := new_port;
                    server_changed := true;
                  end;
                model#clear();
                let list_of_games = get_list_of_games() in
                connexion_status#set_stock `YES;
                List.iter (fun (gamename,mapname,host,nb_pl, nb_max, pwd) ->
                    let row = model#append () in
                    model#set ~row ~column:col_gamename gamename;
                    model#set ~row ~column:col_mapname mapname;
                    model#set ~row ~column:col_host host;
                    model#set ~row ~column:col_players
                              ((string_of_int nb_pl) ^ "/"
                               ^ (string_of_int nb_max));
                    model#set ~row ~column:col_pwd
                              (if pwd then "Oui" else "Non");
                  ) list_of_games
           ));

    dialog#add_button_stock `CANCEL `CANCEL;
    dialog#add_button_stock `OK `OK;
    ignore (dialog#run());      (* TODO *)
    ()

let new_game multi =
  let ask_options mapname =

    let nb_players = IO.nb_player mapname in
    let char_players = IO.char_players mapname in

    let dialog = GWindow.dialog
        ~title:("Créer une nouvelle partie" ^
                 (if multi then " en réseau" else ""))
        ~modal:true (* freeze the rest of the program *)
        ~allow_grow:true
        ~allow_shrink:false
        ~height:250
        () in
    let scroll = GBin.scrolled_window
        ~hpolicy:`NEVER ~vpolicy:`AUTOMATIC
        ~packing:(dialog#vbox#pack ~expand:true) () in
    let vbox = GPack.vbox ~packing:scroll#add_with_viewport () in

    let frame = GBin.frame ~label:("Partie (carte " ^
                                     (Filename.remove_extension
                                        (Filename.basename mapname) ^ ")"))
        ~border_width:3
        ~packing:vbox#pack () in
    let vvbox = GPack.vbox ~packing:frame#add () in
    let hbox = GPack.hbox ~spacing:7
        ~border_width:6
        ~packing:vvbox#pack () in

    ignore (GMisc.label ~text:"Nom"
              ~packing:(hbox#pack ~expand:false) ());
    let def_name = try
        "Partie de "^(Unix.getenv "USER")
      with _ -> "Ma partie" in
    let entry_gamename = GEdit.entry ~text:def_name
        ~max_length:40
        ~has_frame:true
        ~packing:hbox#add () in
    ignore (GMisc.label
              ~text:"Premier tour"
              ~packing:(hbox#pack ~expand:false) ());
              (* ~packing:hbox#add ()); *)

    (* list of the nth first integers *)
    (* TODO letters for players *)
    let nlist n =
      let rec aux a =
        if a >= n then [] else a :: aux (a+1) in
      aux 0
    in
    let combo_turn = GEdit.combo_box_text
        ~active:0
        ~strings:(List.map string_of_int (nlist nb_players))
        ~packing:(hbox#pack ~expand:false)() in

    let entry_pwd = GEdit.entry
        ~max_length:40
        ~has_frame:true () in

    if multi then begin
        (* Ask optional password in multiplayer mode *)
        let hbox = GPack.hbox ~spacing:7
                              ~border_width:6
                              ~packing:vvbox#add () in
        ignore (GMisc.label ~text:"Mot de passe (optionnel)"
                            ~packing:(hbox#pack ~expand:false) ());
        hbox#pack entry_pwd#coerce;
        ()
      end;

    (* array of useful data to initialize player objects *)
    let tab = Array.make nb_players (GEdit.entry(),GEdit.combo_box_text()) in

    for i = 0 to nb_players-1 do
      let frame = GBin.frame ~label:("Joueur "
                                     ^(String.make 1 (List.nth char_players i)))
          ~border_width:3
          ~packing:vbox#pack () in

      let hbox = GPack.hbox ~spacing:7
          ~border_width:6
          ~packing:frame#add () in

      ignore (GMisc.label ~text:"Nom"
                ~packing:(hbox#pack ~expand:false) ());

      let entry = GEdit.entry ~text:("Joueur "
                                     ^(String.make 1 (List.nth char_players i)))
          ~max_length:40
          ~has_frame:true
          ~packing:hbox#add () in
      let combo = GEdit.combo_box_text
          ~active:0
          ~strings:(if multi then ["Local";"IA Standard";"IA Aléatoire";
                                   "IA Facile";"Ouvert"]
                    else ["Humain";"IA Standard";"IA Aléatoire";"IA Facile";])
          ~packing:(hbox#pack ~expand:false) () in

      (* in multiplayer mode we can't set other player's name *)
      if multi then (
        let (c,_) = combo in
        ignore (c#connect#changed
          ~callback:(fun () ->
            match (GEdit.text_combo_get_active combo) with
            | Some("Local") -> ignore (entry#misc#set_sensitive true)
            | _ -> ignore (entry#misc#set_sensitive false)
               ));
      );
      tab.(i) <- (entry,combo)
    done;

    dialog#add_button_stock `CANCEL `CANCEL;
    dialog#add_button_stock `OK `OK;

    let penguins_pos = ref ( List.rev (IO.penguins_pos mapname)) in

    (* get values from the previous form *)
    match dialog#run() with
    | `OK ->
      (* TODO display letters of the map for the players ('a','c', etc.) *)
      let infos = ref [] in
      let turn = match (GEdit.text_combo_get_active combo_turn) with
        | None -> 0
        | Some(txt) -> int_of_string txt
      in
      let gamename = match entry_gamename#text with
        | "" -> def_name
        | s -> s
      in
      for i = nb_players-1 downto 0 do
        let (entry,combo) = tab.(i) in
        let player_type = match (GEdit.text_combo_get_active combo) with
          | None -> "Humain"
          | Some(txt) -> txt
        in
        let player_name = match entry#text with
          | "" -> "Joueur "^(String.make 1 (List.nth char_players i));
          | s -> s
        in
	match !penguins_pos with
	|[] -> failwith "the number of players does not match"
	|l::q ->
	  infos := (player_name, player_type, l)::!infos;
	  penguins_pos := q
      done;

      if multi
      then begin
          (* TODO : launch network thread, send newgame message *)
          filename := "";
          dialog#destroy();
          ()
        end
      else begin
          gs := Some (new Game_state.game_state
                          (Game_state.TXT (mapname, gamename, !infos, turn)));
          filename := "";
          dialog#destroy();
          !load_game()
        end
    | _ -> dialog#destroy();
           st_pop();
  in

  st_push "Création d'un nouveau jeu...";
  let filew = GWindow.file_chooser_dialog
      ~action:`OPEN
      ~title:"Ouvrir une carte" ~border_width:0
      ~width:320 ~height:240
      () in
  filew#add_filter (GFile.filter ~name:"txt" ~patterns:["*.txt"] ());
  filew#add_button_stock `CANCEL `CANCEL;
  filew#add_select_button_stock `OPEN `OPEN;
  begin match filew#run(), filew#filename with
    | `OPEN, Some filename ->
      filew#destroy ();
      _log ("creating new game from map file " ^ filename);
      (try ask_options filename;
       with
         Invalid_argument s
       | Sys_error s ->
         st_pop(); st_flash ~delay:7000 ("Erreur de chargement : " ^ s);
         _log ("loading failed: " ^ s);
       (* | _ -> *)
       (*   st_pop(); *)
       (*   st_pop(); st_flash ~delay:7000 ("Erreur de chargement"); *)
       (*   _log ("loading failed"); *)
      )
    | _ -> filew#destroy (); _log "cancelled";
      st_pop();
  end
